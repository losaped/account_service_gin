package main

import (
	"context"
	"os"
	"os/signal"
	"syscall"
	"time"

	log "github.com/sirupsen/logrus"

	nethttp "net/http"

	"bitbucket.org/losaped/account_service/rpc"
	"bitbucket.org/losaped/account_service/service"
	"bitbucket.org/losaped/account_service/store/inmemory"
	"bitbucket.org/losaped/account_service_gin/http"
)

// @title Account service API
// @version 0.0.1
// @description This is a sample server celler server.
// @termsOfService http://swagger.io/terms/

// @contact.name API Support
// @contact.url http://www.swagger.io/support
// @contact.email support@swagger.io

// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html
func main() {

	mailSender := rpc.NewMailSender("localhost:3131")

	svc := &service.Service{
		Accounts:   inmemory.NewAccountsStore(),
		MailSender: mailSender,
	}

	server := http.CreateAPI(svc, ":3000")
	go func() {
		if err := server.ListenAndServe(); err != nil && err != nethttp.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	quit := make(chan os.Signal, 1)

	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	log.Println("Shutdown Server ...")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := server.Shutdown(ctx); err != nil {
		log.Fatal("Server Shutdown: ", err)
	}

	log.Println("Server stopped")
}
