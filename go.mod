module bitbucket.org/losaped/account_service_gin

go 1.12

require (
	bitbucket.org/losaped/account_service v0.0.0-20190531072828-5b72c87d8e3d
	github.com/alecthomas/template v0.0.0-20160405071501-a0175ee3bccc
	github.com/ghodss/yaml v1.0.0 // indirect
	github.com/gin-gonic/gin v1.4.0
	github.com/pkg/errors v0.8.1 // indirect
	github.com/satori/go.uuid v1.2.0 // indirect
	github.com/shopspring/decimal v0.0.0-20180709203117-cd690d0c9e24 // indirect
	github.com/sirupsen/logrus v1.4.2
	github.com/swaggo/gin-swagger v1.0.0
	github.com/swaggo/swag v1.4.0
	github.com/urfave/cli v1.20.0 // indirect
	golang.org/x/tools v0.0.0-20190322203728-c1a832b0ad89 // indirect
)
