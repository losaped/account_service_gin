package http

import (
	"net/http"

	"fmt"

	"bitbucket.org/losaped/account_service/service"
	"bitbucket.org/losaped/account_service/store"
	"bitbucket.org/losaped/account_service_gin/docs"
	"bitbucket.org/losaped/account_service_gin/http/models"
	"github.com/gin-gonic/gin"
	ginSwagger "github.com/swaggo/gin-swagger" // gin-swagger middleware
	"github.com/swaggo/gin-swagger/swaggerFiles"
)

// NewError example
func NewError(ctx *gin.Context, status int, err error) {
	er := models.HTTPError{
		Code:    status,
		Message: err.Error(),
	}
	ctx.JSON(status, er)
}

type api struct {
	service *service.Service
}

func CreateAPI(svc *service.Service, addr string) *http.Server {
	api := &api{
		service: svc,
	}

	docs.SwaggerInfo.Host = fmt.Sprintf(addr)

	r := gin.Default()

	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	r.POST("/create", api.CreateAccount)

	return &http.Server{
		Addr:    addr,
		Handler: r,
	}
}

// CreateAccount godoc
// @Summary Create new account
// @Description add by json account
// @Tags accounts
// @Accept  json
// @Produce  json
// @Param account body models.CreateAccountRequest true "Add account"
// @Success 200 {object} models.CreateAccountResponse
// @Failure 400 {object} models.HTTPError
// @Failure 500 {object} models.HTTPError
// @Router /create [post]
func (api *api) CreateAccount(c *gin.Context) {
	var req models.CreateAccountRequest
	if err := c.ShouldBindJSON(&req); err != nil {
		NewError(c, http.StatusBadRequest, err)
		return
	}

	newAccount := store.Account{
		Email:    req.Email,
		Password: req.Password,
		Phone:    req.Phone,
	}

	createdAcc, err := api.service.CreateAccount(newAccount)
	if err != nil {
		NewError(c, http.StatusInternalServerError, err)
		return
	}

	c.JSON(http.StatusOK, *createdAcc)
}
