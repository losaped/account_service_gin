package models

import "errors"

var (
	ErrEmailIsInvalid  = errors.New("email is empty")
	ErrPhoneIsEmpty    = errors.New("phone is empty")
	ErrPasswordIsEmpty = errors.New("password is empty")
)

// CreateAccountRequest account request
type CreateAccountRequest struct {
	Email    string `json:"email"`
	Phone    string `json:"phone"`
	Password string `json:"password"`
}

func (r CreateAccountRequest) Validation() error {
	switch {
	case len(r.Email) == 0:
		return ErrEmailIsInvalid
	case len(r.Phone) == 0:
		return ErrPhoneIsEmpty
	case len(r.Password) == 0:
		return ErrPasswordIsEmpty
	default:
		return nil
	}
}

// CreateAccountResponse returns after account created
type CreateAccountResponse struct {
	ID    string `json:"id"`
	Email string `json:"email"`
	Phone string `json:"phone"`
}

// HTTPError example
type HTTPError struct {
	Code    int    `json:"code" example:"400"`
	Message string `json:"message" example:"status bad request"`
}
